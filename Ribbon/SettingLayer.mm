//
//  SettingLayer.mm
//  Ribbon
//
//  Created by Esa Firman on 1/30/14.
//  Copyright 2014 Dycode. All rights reserved.
//

#import "SettingLayer.h"
#import "SimpleAudioEngine.h"

@interface SettingLayer()
@property BOOL fbIsOn;
@property BOOL twIsOn;
@end
@implementation SettingLayer
-(id) init
{
	if ((self = [super init])) {
        CGSize size = [[CCDirector sharedDirector] winSize];
        CCSprite *bg = [CCSprite spriteWithFile:@"BackgroundSetting.png"];
        bg.position = ccp(size.width/2, size.height/2);
        [self addChild:bg z:-1];
        [self setUpMenus];
        self.fbIsOn = NO;
        self.twIsOn = NO;
    }
    
    return self;
}
-(void) setUpMenus
{
    
	CCMenuItemImage *menuItem3 = [CCMenuItemImage itemWithNormalImage:@"ButtonMute.png" selectedImage:@"ButtonMute.png" block:^(id sender) {
        [SimpleAudioEngine sharedEngine].mute = ![SimpleAudioEngine sharedEngine].mute;
        CCMenuItemImage *test = (CCMenuItemImage *)sender;
        if ([SimpleAudioEngine sharedEngine].mute) {
            CCSprite *frame = [CCSprite spriteWithFile:@"ButtonSound.png"];
            [test setNormalImage:frame];
        }else
        {
            CCSprite *frame = [CCSprite spriteWithFile:@"ButtonMute.png"];
            [test setNormalImage:frame];
        }
    }];
 
	CCMenu * myMenu = [CCMenu menuWithItems: menuItem3, nil];
    [myMenu alignItemsVerticallyWithPadding:10];
    CGSize size = [[CCDirector sharedDirector] winSize];
    [myMenu setPosition:ccp(size.width/2, size.height *0.75)];
	[self addChild:myMenu];

    CCMenuItemImage *menuItem0 = [CCMenuItemImage itemWithNormalImage:@"ButtonClose.png" selectedImage:@"ButtonClose.png" block:^(id sender) {
        [self removeFromParentAndCleanup:YES];
        [[CCDirector sharedDirector] resume];
    }];
    CCMenu * myMenus = [CCMenu menuWithItems: menuItem0, nil];
    [myMenus setPosition:ccp(size.width*0.9, size.height*0.9)];
    [self addChild:myMenus];
    
    CCLabelTTF *label = [CCLabelTTF labelWithString:@"Sound" fontName:@"Marker Felt" fontSize:32];
    [label setColor:ccc3(255,255,255)];
    label.position = ccp( size.width/2, size.height*0.9);
    [self addChild:label z:0];
    
    CCLabelTTF *label2 = [CCLabelTTF labelWithString:@"Share" fontName:@"Marker Felt" fontSize:32];
    [label2 setColor:ccc3(255,255,255)];
    label2.position = ccp( size.width/2, size.height*0.5);
    [self addChild:label2 z:0];
    
    CCLabelTTF *labelFB = [CCLabelTTF labelWithString:@"Facebook" fontName:@"Marker Felt" fontSize:22];
    [labelFB setColor:ccc3(255,255,255)];
    labelFB.position = ccp( size.width/2 - labelFB.boundingBox.size.width/2, size.height*0.4);
    [self addChild:labelFB z:0];
    
    CCLabelTTF *labelTW = [CCLabelTTF labelWithString:@"Twitter" fontName:@"Marker Felt" fontSize:22];
    [labelTW setColor:ccc3(255,255,255)];
    labelTW.position = ccp( size.width/2 - labelTW.boundingBox.size.width/2, size.height*0.3);
    [self addChild:labelTW z:0];
    
    CCMenuItemImage *menuItemFB = [CCMenuItemImage itemWithNormalImage:@"RadioButtonOff.png" selectedImage:@"RadioButtonOff.png" block:^(id sender) {
        self.fbIsOn = !self.fbIsOn;
        CCMenuItemImage *test = (CCMenuItemImage *)sender;
        if (self.fbIsOn) {
            CCSprite *frame = [CCSprite spriteWithFile:@"RadioButton.png"];
            [test setNormalImage:frame];
        }else
        {
            CCSprite *frame = [CCSprite spriteWithFile:@"RadioButtonOff.png"];
            [test setNormalImage:frame];
        }
    }];
    
    CCMenuItemImage *menuItemTW = [CCMenuItemImage itemWithNormalImage:@"RadioButtonOff.png" selectedImage:@"RadioButtonOff.png" block:^(id sender) {
        self.twIsOn = !self.twIsOn;
        CCMenuItemImage *test = (CCMenuItemImage *)sender;
        if (self.twIsOn) {
            CCSprite *frame = [CCSprite spriteWithFile:@"RadioButton.png"];
            [test setNormalImage:frame];
        }else
        {
            CCSprite *frame = [CCSprite spriteWithFile:@"RadioButtonOff.png"];
            [test setNormalImage:frame];
        }
    }];
    
	CCMenu * myMenuShare = [CCMenu menuWithItems: menuItemFB, menuItemTW, nil];
    [myMenuShare alignItemsVerticallyWithPadding:10];
    [myMenuShare setPosition:ccp(size.width/2 + 40, size.height *0.35)];
	[self addChild:myMenuShare];
}
@end
