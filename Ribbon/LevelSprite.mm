//
//  LevelSprite.mm
//  Ribbon
//
//  Created by Esa Firman on 1/29/14.
//  Copyright 2014 Dycode. All rights reserved.
//

#import "LevelSprite.h"


@implementation LevelSprite
+ (CCSprite *)spriteWithText:(NSString *)text andCleared:(BOOL)cleared
{
    CCSprite *sprite;
    if (cleared) {
        sprite = [CCSprite spriteWithFile:@"LevelDone3.png"];
    }else
    {
        sprite = [CCSprite spriteWithFile:@"LevelRed.png"];
    }

    CCLabelTTF *label = [CCLabelTTF labelWithString:text fontName:@"Marker Felt" fontSize:12];
    [sprite addChild:label z:100];
    [label setColor:ccc3(255,255,255)];
    label.position = ccp( [sprite boundingBox].size.width/2, [sprite boundingBox].size.height/2);
    return sprite;
}
@end
