//
//  LevelsLayer.m
//  Ribbon
//
//  Created by Esa Firman on 1/29/14.
//  Copyright 2014 Dycode. All rights reserved.
//

#import "LevelsLayer.h"
#import "HelloWorldLayer.h"
#import "LevelSprite.h"
@interface LevelsLayer()
@property NSMutableArray * levels1;
@property NSMutableArray * levels2;
@property NSMutableArray * levels3;
@end

@implementation LevelsLayer
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	LevelsLayer *layer = [LevelsLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}
-(id) init
{
	if( (self=[super init])) {
		
		// ask director for the window size
		CGSize size = [[CCDirector sharedDirector] winSize];
		
		CCSprite *background;
        background = [CCSprite spriteWithFile:@"BgLevel.png"];
		background.position = ccp(size.width/2, size.height/2);
		[self addChild:background z:-1];
        
        CCLabelTTF *label = [CCLabelTTF labelWithString:@"Levels" fontName:@"Marker Felt" fontSize:32];
		[label setColor:ccc3(255,255,255)];
		label.position = ccp( size.width/2, size.height*0.9);
        [self addChild:label z:0];
        
        CCMenuItemImage *menuItem1 = [CCMenuItemImage itemWithNormalImage:@"ButtonBack.png"
                                                            selectedImage:@"ButtonBack.png"
                                                                   target:self
                                                                 selector:@selector(backClicked)];
        CCMenu * myMenu = [CCMenu menuWithItems: menuItem1, nil];
        [myMenu setPosition:ccp(size.width*0.1, size.height*0.9)];
        [self addChild:myMenu];
        
        self.levels1 = [[NSMutableArray alloc]init];
        self.levels2 = [[NSMutableArray alloc]init];
        self.levels3 = [[NSMutableArray alloc]init];
        
        [self setUpLevels];
	}
	
	return self;
}
-(void) setUpLevels
{
    CGSize size = [[CCDirector sharedDirector] winSize];
    for (int i=0; i<15; i++) {
        
        CCSprite *sprite = [[CCSprite alloc]init];
        if (i==0) {
            sprite = [LevelSprite spriteWithText:[NSString stringWithFormat:@"%d",i+1] andCleared:YES];
        }else
        {
            sprite = [LevelSprite spriteWithText:[NSString stringWithFormat:@"%d",i+1] andCleared:NO];
        }
        
        if (i%2==0) {
            sprite.rotation = -5;
        }else{
            sprite.rotation = 5;
        }
        CCMenuItemSprite *menuItem = [CCMenuItemSprite itemWithNormalSprite:sprite selectedSprite:nil disabledSprite:nil block:^(id sender) {
            [self levelSelected:i+1];
        }];
        
//        CCMenuItemImage *menuItem = [CCMenuItemImage itemWithNormalImage:@"ButtonClose.png"
//                                                           selectedImage:@"ButtonClose.png"
//                                                                   block:^(id sender) {
//                                                                       [self levelSelected:i+1];
//                                                                   }];
//        CCMenuItemImage *menuItem = [CCMenuItemImage itemWithNormalImage:@"ButtonClose.png"
//                                                           selectedImage:@"ButtonClose.png"
//                                                                  target:self
//                                                                selector:@selector(backClicked)];
        [self.levels1 addObject:menuItem];
    }
    
    CCMenu * myMenu = [CCMenu menuWithArray:self.levels1];
//    [myMenu alignItemsHorizontallyWithPadding:10];
    NSNumber* itemsPerRow = [NSNumber numberWithInt:5];
    [myMenu alignItemsInColumns:itemsPerRow, itemsPerRow, itemsPerRow, nil];
    [myMenu setPosition:ccp(size.width*0.5, size.height*0.4)];
    [self addChild:myMenu];
}
#pragma mark - Actions
-(void) backClicked
{
    [[CCDirector sharedDirector] popScene];
}
-(void) levelSelected:(NSInteger)level
{
    NSLog(@"LEVEL %d",level);
    if (level == 1) {
        [[CCDirector sharedDirector] pushScene:[CCTransitionFade transitionWithDuration:1.0 scene:[HelloWorldLayer scene] ]];
    }
}
@end
