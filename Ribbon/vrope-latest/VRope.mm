//
//  VRope.m
//
//  Created by patrick on 16/10/2010.
//

#import "VRope.h"


@implementation VRope
@synthesize sticks = vSticks;
#ifdef BOX2D_H
-(id)initWithRopeJoint:(b2RopeJoint*)aJoint spriteSheet:(CCSpriteBatchNode*)spriteSheetArg {
    if((self = [super init])) {
        joint = aJoint;
        CGPoint pointA = ccp(joint->GetAnchorA().x*PTM_RATIO,joint->GetAnchorA().y*PTM_RATIO);
        CGPoint pointB = ccp(joint->GetAnchorB().x*PTM_RATIO,joint->GetAnchorB().y*PTM_RATIO);
        spriteSheet = spriteSheetArg;
        [self createRope:pointA pointB:pointB distance:joint->GetMaxLength()*PTM_RATIO];
    }
    return self;
}
-(id)initWithRopeJoint:(b2RopeJoint*)aJoint
           spriteSheet:(CCSpriteBatchNode*)spriteSheetArg
                points:(NSArray*)points
                sticks:(NSArray*)sticks
               sprites:(NSArray*)sprites {
    if((self = [super init])) {
        joint = aJoint;
        spriteSheet = spriteSheetArg;
        vPoints = [[NSMutableArray alloc] initWithArray:points];
        vSticks = [[NSMutableArray alloc] initWithArray:sticks];
        ropeSprites = [[NSMutableArray alloc] initWithArray:sprites];
        numPoints = vPoints.count;
    }
    return self;
}
-(void)reset {
    CGPoint pointA = ccp(joint->GetAnchorA().x*PTM_RATIO,joint->GetAnchorA().y*PTM_RATIO);
    CGPoint pointB = ccp(joint->GetAnchorB().x*PTM_RATIO,joint->GetAnchorB().y*PTM_RATIO);
    [self resetWithPoints:pointA pointB:pointB];
}

-(void)update:(float)dt {
    CGPoint pointA = ccp(joint->GetAnchorA().x*PTM_RATIO,joint->GetAnchorA().y*PTM_RATIO);
    CGPoint pointB = ccp(joint->GetAnchorB().x*PTM_RATIO,joint->GetAnchorB().y*PTM_RATIO);
    [self updateWithPoints:pointA pointB:pointB dt:dt];
}
#endif

-(id)initWithPoints:(CGPoint)pointA pointB:(CGPoint)pointB spriteSheet:(CCSpriteBatchNode*)spriteSheetArg {
	if((self = [super init])) {
		spriteSheet = spriteSheetArg;
		[self createRope:pointA pointB:pointB distance:ccpDistance(pointA, pointB)];
	}
	return self;
}

-(void)createRope:(CGPoint)pointA pointB:(CGPoint)pointB distance:(float)distance {
	vPoints = [[NSMutableArray alloc] init];
	vSticks = [[NSMutableArray alloc] init];
	ropeSprites = [[NSMutableArray alloc] init];
//	float distance = ccpDistance(pointA,pointB);
	int segmentFactor = 12; //increase value to have less segments per rope, decrease to have more segments
	numPoints = distance/segmentFactor;
	CGPoint diffVector = ccpSub(pointB,pointA);
	float multiplier = distance / (numPoints-1);
	antiSagHack = 0.1f; //HACK: scale down rope points to cheat sag. set to 0 to disable, max suggested value 0.1
	for(int i=0;i<numPoints;i++) {
		CGPoint tmpVector = ccpAdd(pointA, ccpMult(ccpNormalize(diffVector),multiplier*i*(1-antiSagHack)));
		VPoint *tmpPoint = [[VPoint alloc] init];
		[tmpPoint setPos:tmpVector.x y:tmpVector.y];
		[vPoints addObject:tmpPoint];
	}
	for(int i=0;i<numPoints-1;i++) {
		VStick *tmpStick = [[VStick alloc] initWith:[vPoints objectAtIndex:i] pointb:[vPoints objectAtIndex:i+1]];
		[vSticks addObject:tmpStick];
	}
	if(spriteSheet!=nil) {
		for(int i=0;i<numPoints-1;i++) {
			VPoint *point1 = [[vSticks objectAtIndex:i] getPointA];
			VPoint *point2 = [[vSticks objectAtIndex:i] getPointB];
			CGPoint stickVector = ccpSub(ccp(point1.x,point1.y),ccp(point2.x,point2.y));
			float stickAngle = ccpToAngle(stickVector);
//			CCSprite *tmpSprite = [CCSprite spriteWithBatchNode:spriteSheet rect:CGRectMake(0,0,multiplier,[[[spriteSheet textureAtlas] texture] pixelsHigh])];
            CCSprite *tmpSprite = [CCSprite spriteWithTexture:spriteSheet.texture
                                                         rect:CGRectMake(0,0,
                                                                         multiplier,
                                                                         [[[spriteSheet textureAtlas] texture] pixelsHigh]/CC_CONTENT_SCALE_FACTOR())];

			ccTexParams params = {GL_LINEAR,GL_LINEAR,GL_REPEAT,GL_REPEAT};
			[tmpSprite.texture setTexParameters:&params];
			[tmpSprite setPosition:ccpMidpoint(ccp(point1.x,point1.y),ccp(point2.x,point2.y))];
			[tmpSprite setRotation:-1 * CC_RADIANS_TO_DEGREES(stickAngle)];
			[spriteSheet addChild:tmpSprite];
			[ropeSprites addObject:tmpSprite];
		}
	}
}

-(void)resetWithPoints:(CGPoint)pointA pointB:(CGPoint)pointB {
	float distance = ccpDistance(pointA,pointB);
	CGPoint diffVector = ccpSub(pointB,pointA);
	float multiplier = distance / (numPoints - 1);
	for(int i=0;i<numPoints;i++) {
		CGPoint tmpVector = ccpAdd(pointA, ccpMult(ccpNormalize(diffVector),multiplier*i*(1-antiSagHack)));
		VPoint *tmpPoint = [vPoints objectAtIndex:i];
		[tmpPoint setPos:tmpVector.x y:tmpVector.y];
		
	}
}

-(void)removeSprites {
	for(int i=0;i<numPoints-1;i++) {
		CCSprite *tmpSprite = [ropeSprites objectAtIndex:i];
		[spriteSheet removeChild:tmpSprite cleanup:YES];
	}
	[ropeSprites removeAllObjects];
	[ropeSprites release];
}

-(void)updateWithPoints:(CGPoint)pointA pointB:(CGPoint)pointB dt:(float)dt {
	//manually set position for first and last point of rope
	[[vPoints objectAtIndex:0] setPos:pointA.x y:pointA.y];
	[[vPoints objectAtIndex:numPoints-1] setPos:pointB.x y:pointB.y];
	
	//update points, apply gravity
	for(int i=1;i<numPoints-1;i++) {
//		[[vPoints objectAtIndex:i] applyGravity:dt];
		[[vPoints objectAtIndex:i] update];
	}
	
	//contract sticks
	int iterations = 4;
	for(int j=0;j<iterations;j++) {
		for(int i=0;i<numPoints-1;i++) {
			[[vSticks objectAtIndex:i] contract];
		}
	}
}

-(void)updateSprites {
	if(spriteSheet!=nil) {
		for(int i=0;i<numPoints-1;i++) {
			VPoint *point1 = [[vSticks objectAtIndex:i] getPointA];
			VPoint *point2 = [[vSticks objectAtIndex:i] getPointB];
			CGPoint point1_ = ccp(point1.x,point1.y);
			CGPoint point2_ = ccp(point2.x,point2.y);
			float stickAngle = ccpToAngle(ccpSub(point1_,point2_));
			CCSprite *tmpSprite = [ropeSprites objectAtIndex:i];
			[tmpSprite setPosition:ccpMidpoint(point1_,point2_)];
			[tmpSprite setRotation: -CC_RADIANS_TO_DEGREES(stickAngle)];
		}
	}	
}

//-(void)debugDraw {
//	//Depending on scenario, you might need to have different Disable/Enable of Client States
//	//glDisableClientState(GL_TEXTURE_2D);
//	//glDisableClientState(GL_TEXTURE_COORD_ARRAY);
//	//glDisableClientState(GL_COLOR_ARRAY);
//	//set color and line width for ccDrawLine
//	glColor4f(0.0f,0.0f,1.0f,1.0f);
//	glLineWidth(5.0f);
//	for(int i=0;i<numPoints-1;i++) {
//		//"debug" draw
//		VPoint *pointA = [[vSticks objectAtIndex:i] getPointA];
//		VPoint *pointB = [[vSticks objectAtIndex:i] getPointB];
//		ccDrawPoint(ccp(pointA.x,pointA.y));
//		ccDrawPoint(ccp(pointB.x,pointB.y));
//		//ccDrawLine(ccp(pointA.x,pointA.y),ccp(pointB.x,pointB.y));
//	}
//	//restore to white and default thickness
//	glColor4f(1.0f,1.0f,1.0f,1.0f);
//	glLineWidth(1);
//	//glEnableClientState(GL_TEXTURE_2D);
//	//glEnableClientState(GL_TEXTURE_COORD_ARRAY);
//	//glEnableClientState(GL_COLOR_ARRAY);
//}

-(void)dealloc {
	for(int i=0;i<numPoints;i++) {
		[[vPoints objectAtIndex:i] release];
		if(i!=numPoints-1)
			[[vSticks objectAtIndex:i] release];
	}
	[vPoints removeAllObjects];
	[vSticks removeAllObjects];
	[vPoints release];
	[vSticks release];
	[super dealloc];
}
-(VRope *)cutRopeInStick:(VStick *)stick newBodyA:(b2Body*)newBodyA newBodyB:(b2Body*)newBodyB {
    
    // 1-First, find out where in your array the rope will be cut
    int nPoint = [vSticks indexOfObject:stick];
    
    // Instead of making everything again you'll just use the arrays of
    // sticks, points and sprites you already have and split them
    
    // 2-This is the range that defines the new rope
    NSRange newRopeRange = (NSRange){nPoint, numPoints-nPoint-1};
    
    // 3-Keep the sticks in a new array
    NSArray *newRopeSticks = [vSticks subarrayWithRange:newRopeRange];
    
    // 4-and remove from this object's array
    [vSticks removeObjectsInRange:newRopeRange];
    
    // 5-Same for the sprites
    NSArray *newRopeSprites = [ropeSprites subarrayWithRange:newRopeRange];
    [ropeSprites removeObjectsInRange:newRopeRange];
    
    // 6-Number of points is always the number of sticks + 1
    newRopeRange.length += 1;
    NSArray *newRopePoints = [vPoints subarrayWithRange:newRopeRange];
    [vPoints removeObjectsInRange:newRopeRange];
    
    // 7-The removeObjectsInRange above removed the last point of
    // this rope that now belongs to the new rope. You need to clone
    // that VPoint and add it to this rope, otherwise you'll have a
    // wrong number of points in this rope
    VPoint *pointOfBreak = [newRopePoints objectAtIndex:0];
    VPoint *newPoint = [[VPoint alloc] init];
    [newPoint setPos:pointOfBreak.x y:pointOfBreak.y];
    [vPoints addObject:newPoint];
    
    // 7-And last: fix the last VStick of this rope to point to this new point
    // instead of the old point that now belongs to the new rope
    VStick *lastStick = [vSticks lastObject];
    [lastStick setPointB:newPoint];
    [newPoint release];
    
    // 8-This will determine how long the rope is now and how long the new rope will be
    float32 cutRatio = (float32)nPoint / (numPoints - 1);
    
    // 9-Fix my number of points
    numPoints = nPoint + 1;
    
    // Position in Box2d world where the new bodies will initially be
    b2Vec2 newBodiesPosition = b2Vec2(pointOfBreak.x / PTM_RATIO, pointOfBreak.y / PTM_RATIO);
    
    // Get a reference to the world to create the new joint
    b2World *world = newBodyA->GetWorld();
    
    // 10-Re-create the joint used in this VRope since bRopeJoint does not allow
    // to re-define the attached bodies
    b2RopeJointDef jd;
    jd.bodyA = joint->GetBodyA();
    jd.bodyB = newBodyB;
    jd.localAnchorA = joint->GetLocalAnchorA();
    jd.localAnchorB = b2Vec2(0, 0);
    jd.maxLength = joint->GetMaxLength() * cutRatio;
    newBodyB->SetTransform(newBodiesPosition, 0.0);
    
    b2RopeJoint *newJoint1 = (b2RopeJoint *)world->CreateJoint(&jd); //create joint
    
    // 11-Create the new rope joint
    jd.bodyA = newBodyA;
    jd.bodyB = joint->GetBodyB();
    jd.localAnchorA = b2Vec2(0, 0);
    jd.localAnchorB = joint->GetLocalAnchorB();
    jd.maxLength = joint->GetMaxLength() * (1 - cutRatio);
    newBodyA->SetTransform(newBodiesPosition, 0.0);
    
    b2RopeJoint *newJoint2 = (b2RopeJoint *)world->CreateJoint(&jd); //create joint
    
    // 12-Destroy the old joint and update to the new one
    world->DestroyJoint(joint);
    joint = newJoint1;
    
    // 13-Finally, create the new VRope
    VRope *newRope = [[VRope alloc] initWithRopeJoint:newJoint2
                                          spriteSheet:spriteSheet
                                               points:newRopePoints
                                               sticks:newRopeSticks
                                              sprites:newRopeSprites];
    return [newRope autorelease];
}
@end
