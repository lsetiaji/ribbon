//
//  MainMenuLayer.mm
//  Ribbon
//
//  Created by Esa Firman on 1/29/14.
//  Copyright 2014 Dycode. All rights reserved.
//

#import "MainMenuLayer.h"
#import "AboutLayer.h"
#import "LevelsLayer.h"
#import "SettingLayer.h"
#import "HowToPlayLayer.h"
#import "SimpleAudioEngine.h"
@implementation MainMenuLayer
// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	MainMenuLayer *layer = [MainMenuLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}
-(id) init
{
	if( (self=[super init])) {
		
		// ask director for the window size
		CGSize size = [[CCDirector sharedDirector] winSize];
		
		CCSprite *background;
        background = [CCSprite spriteWithFile:@"BackgroundHome.png"];
		background.position = ccp(size.width/2, size.height/2);
        
        CCSprite *logo;
        logo = [CCSprite spriteWithFile:@"LogoRibbon.png"];
		logo.position = ccp(size.width/2, size.height*0.5);
        
        CCSprite *score;
        score = [CCSprite spriteWithFile:@"Score.png"];
		score.position = ccp(size.width*0.475, size.height*0.9);
        
		
		// add the label as a child to this Layer
        [self addChild:background z:-1];
        [self addChild:logo z:0];
        [self addChild:score z:0];
        [self setUpMenus];
        
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"background-music-aac.caf"];
	}
	
	return self;
}
-(void) setUpMenus
{
	// Create some menu items
    CCMenuItemImage *menuItem1 = [CCMenuItemImage itemWithNormalImage:@"ButtonHowToPlay.png"
                                                        selectedImage:@"ButtonHowToPlay.png"
                                                               target:self
                                                             selector:@selector(howToPlayClicked)];
    CCMenuItemImage *menuItem2 = [CCMenuItemImage itemWithNormalImage:@"ButtonSetting.png"
                                                        selectedImage:@"ButtonSetting.png"
                                                               target:self
                                                             selector:@selector(settingClicked)];
    CCMenuItemImage *menuItem3 = [CCMenuItemImage itemWithNormalImage:@"About.png"
                                                        selectedImage:@"About.png"
                                                               target:self
                                                             selector:@selector(aboutClicked)];
    
    CCMenuItemImage *menuItem4 = [CCMenuItemImage itemWithNormalImage:@"ButtonPlay.png"
                                                        selectedImage:@"ButtonPlay.png"
                                                               target:self
                                                             selector:@selector(playClicked)];
    
//	CCMenuItem * menuItem4 = [CCMenuItemFont itemWithString:@"Menu" block:^(id sender) {
//        NSLog(@"MENU CLICKED");
//        MenuLayer *menu = [[MenuLayer alloc]init];
//        [self addChild:menu];
//    }];
//    [menuItem2 setColor:ccRED];
//
    
	// Create a menu and add your menu items to it
	CCMenu * myMenu = [CCMenu menuWithItems: menuItem1,menuItem2,menuItem3, nil];
//    [CCMenuItemFont setFontSize:10];
    
	// Arrange the menu items vertically
    CGSize size = [[CCDirector sharedDirector] winSize];
    [myMenu alignItemsHorizontallyWithPadding:10];
    
    [myMenu setPosition:ccp( size.width/2, size.height/8)];
	// add the menu to your scene
	[self addChild:myMenu];
    
    CCMenu * myMenu2 = [CCMenu menuWithItems: menuItem4, nil];
    [myMenu2 setPosition:ccp( size.width/2, size.height*0.35)];
    [self addChild:myMenu2];
}
#pragma mark - Actions
-(void)playClicked
{
    [[CCDirector sharedDirector] pushScene:[CCTransitionFade transitionWithDuration:1.0 scene:[LevelsLayer scene] ]];
//    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:1.0 scene:[HelloWorldLayer scene] ]];
}
-(void)aboutClicked{
    [[CCDirector sharedDirector] pushScene:[CCTransitionFade transitionWithDuration:1.0 scene:[AboutLayer scene] ]];
}
-(void)settingClicked{
    [[CCDirector sharedDirector] pause];
    SettingLayer *menu = [[SettingLayer alloc]init];
    [self addChild:menu];
}
-(void)howToPlayClicked
{
    [[CCDirector sharedDirector] pushScene:[CCTransitionFade transitionWithDuration:1.0 scene:[HowToPlayLayer scene] ]];
}
@end
