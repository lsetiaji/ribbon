//
//  HowToPlayLayer.mm
//  Ribbon
//
//  Created by Esa Firman on 1/30/14.
//  Copyright 2014 Dycode. All rights reserved.
//

#import "HowToPlayLayer.h"


@implementation HowToPlayLayer
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	HowToPlayLayer *layer = [HowToPlayLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}
-(id) init
{
	if ((self = [super init])) {
        CGSize size = [[CCDirector sharedDirector] winSize];
        
        CCSprite *background = [CCSprite spriteWithFile:@"BgLevel.png"];
        background.anchorPoint = CGPointZero;
        [self addChild:background z:-1];
        
        CCMenuItemImage *menuItem1 = [CCMenuItemImage itemWithNormalImage:@"ButtonBack.png"
                                                            selectedImage:@"ButtonBack.png"
                                                                   target:self
                                                                 selector:@selector(backClicked)];
        CCMenu * myMenu = [CCMenu menuWithItems: menuItem1, nil];
        [myMenu setPosition:ccp(size.width*0.1, size.height*0.9)];
        [self addChild:myMenu];
        
        CCMenuItemImage *menuItem2 = [CCMenuItemImage itemWithNormalImage:@"ButtonPlay.png"
                                                            selectedImage:@"ButtonPlay.png"
                                                                   target:self
                                                                 selector:@selector(playClicked)];
        CCMenu * myMenu2 = [CCMenu menuWithItems: menuItem2, nil];
        [myMenu2 setPosition:ccp(size.width*0.5, size.height*0.1)];
        [self addChild:myMenu2];
    }
    return self;
}
#pragma mark - Actions
-(void) backClicked
{
    [[CCDirector sharedDirector] popScene];
}
-(void) playClicked
{
    [[CCDirector sharedDirector] popScene];
}
@end
