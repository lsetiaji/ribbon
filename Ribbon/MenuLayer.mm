//
//  MenuLayer.mm
//  Ribbon
//
//  Created by Esa Firman on 1/29/14.
//  Copyright 2014 Dycode. All rights reserved.
//

#import "MenuLayer.h"
#import "LevelsLayer.h"
#import "SimpleAudioEngine.h"
#import "HowToPlayLayer.h"
@implementation MenuLayer
-(id) init
{
	if ((self = [super init])) {
        CGSize size = [[CCDirector sharedDirector] winSize];
//        CCLayerColor *layer = [[CCLayerColor alloc]
//                               initWithColor:ccc4(0,0,0,100) width:size.width height:size.height];
//        [self addChild:layer z:-1];
        CCSprite *bg = [CCSprite spriteWithFile:@"BgPause.png"];
        bg.position = ccp(size.width/2, size.height/2);
        [self addChild:bg z:-1];
        [self setUpMenus];
    }
    
    return self;
}
-(void) setUpMenus
{
    
	// Create some menu items
    CCMenuItemImage *menuItem1 = [CCMenuItemImage itemWithNormalImage:@"ButtonHowToPlay2.png" selectedImage:@"ButtonHowToPlay2.png" block:^(id sender) {
        NSLog(@"MENU CLICKED");
        [self removeFromParentAndCleanup:YES];
        [[CCDirector sharedDirector] resume];
        [[CCDirector sharedDirector] pushScene:[CCTransitionFade transitionWithDuration:1.0 scene:[HowToPlayLayer scene] ]];
    }];
	 CCMenuItemImage *menuItem2 = [CCMenuItemImage itemWithNormalImage:@"ButtonReturnToMenu.png" selectedImage:@"ButtonReturnToMenu.png" block:^(id sender) {
        CCScene * levelSelector = [CCScene node];
        
        // 'layer' is an autorelease object.
        [self removeFromParentAndCleanup:YES];
        [[CCDirector sharedDirector] resume];
        LevelsLayer *layer = [LevelsLayer node];
        
        // add layer as a child to scene
        [levelSelector addChild: layer];
        [[CCDirector sharedDirector] replaceScene:levelSelector];
    }];
//    CCLabelTTF *labelSound = [CCLabelTTF labelWithString:@"Sound Off" fontName:@"Marker Felt" fontSize:32];
    CCMenuItemImage *menuItem3 = [CCMenuItemImage itemWithNormalImage:@"ButtonMute.png" selectedImage:@"ButtonMute.png" block:^(id sender) {
        [SimpleAudioEngine sharedEngine].mute = ![SimpleAudioEngine sharedEngine].mute;
        CCMenuItemImage *test = (CCMenuItemImage *)sender;
        if ([SimpleAudioEngine sharedEngine].mute) {
            CCSprite *frame = [CCSprite spriteWithFile:@"ButtonSound.png"];
            [test setNormalImage:frame];
//            [labelSound setString:@"Sound On"];
        }else
        {
            CCSprite *frame = [CCSprite spriteWithFile:@"ButtonMute.png"];
            [test setNormalImage:frame];
//            [labelSound setString:@"Sound Off"];
        }
    }];
    
    
    
    CCMenuItemImage *menuItem4 = [CCMenuItemImage itemWithNormalImage:@"ButtonContinue.png" selectedImage:@"ButtonContinue.png" block:^(id sender) {
        [self removeFromParentAndCleanup:YES];
        [[CCDirector sharedDirector] resume];
    }];

    

    
    //    self.anchorPoint = ccp(0,0);
    
	// Create a menu and add your menu items to it
	CCMenu * myMenu = [CCMenu menuWithItems: menuItem3,menuItem2,menuItem1,menuItem4, nil];
    [CCMenuItemFont setFontSize:22];
	// Arrange the menu items vertically
    [myMenu alignItemsVerticallyWithPadding:10];
    CGSize size = [[CCDirector sharedDirector] winSize];
    [myMenu setPosition:ccp(size.width/2, size.height *0.5)];
	// add the menu to your scene
	[self addChild:myMenu];
//    [labelSound setColor:ccc3(255,255,255)];
//    labelSound.position = ccp( menuItem3.boundingBox.size.width, menuItem3.boundingBox.size.height);
//    [self addChild:labelSound z:100];
}
@end
