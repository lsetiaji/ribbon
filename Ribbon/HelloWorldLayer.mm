//
//  HelloWorldLayer.mm
//  Ribbon
//
//  Created by Esa Firman on 1/24/14.
//  Copyright Dycode 2014. All rights reserved.
//

// Import the interfaces
#import "HelloWorldLayer.h"
#import "VRope.h"
// Not included in "cocos2d.h"
#import "CCPhysicsSprite.h"
#import "MenuLayer.h"
// Needed to obtain the Navigation Controller
#import "AppDelegate.h"
//#import "SimpleAudioEngine.h"
#import "LoseLayer.h"

enum {
	kTagParentNode = 1,
};


#pragma mark - HelloWorldLayer

@interface HelloWorldLayer()
-(void) initPhysics;
//-(b2Body *) addNewSpriteAtPosition:(CGPoint)p;
-(void) createMenu;
@property CCSprite *rope;
@property b2Body *theRope;
@property b2Body *theAstro;
@property CGPoint oldCoor;
@property b2RopeJoint *joints;
@property int counter;
@property int lives;
@property NSMutableArray * monsters;
@property NSMutableArray * tails;
@property CCLabelTTF *labelScore;
@property CCMotionStreak *missileStreak;
@end

@implementation HelloWorldLayer

+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	HelloWorldLayer *layer = [HelloWorldLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}
-(b2Body *) createCandyAt:(CGPoint)pt
{
    // Get the sprite from the sprite sheet
    
    CCSprite *sprite = [CCSprite spriteWithFile:@"astronaut.png"];
    [sprite addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(playerDragged:)]];
    sprite.isTouchEnabled = NO;
    [self addChild:sprite];
    self.rope = sprite;
    // Defines the body of your candy
    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    bodyDef.position = b2Vec2(pt.x/PTM_RATIO, pt.y/PTM_RATIO);
    bodyDef.userData = sprite;
    bodyDef.linearDamping = 0.0f;
    bodyDef.angularDamping = 0.0f;
//    bodyDef.fixedRotation = YES;
    b2Body *body = world->CreateBody(&bodyDef);
    
    // Define the fixture as a polygon
    b2FixtureDef fixtureDef;
    b2PolygonShape spriteShape;

    
    b2Vec2 verts[] = {
        b2Vec2(-1.0f / PTM_RATIO, -38.0f / PTM_RATIO),
        b2Vec2(35.5f / PTM_RATIO, -18.0f / PTM_RATIO),
        b2Vec2(1.5f / PTM_RATIO, 38.0f / PTM_RATIO),
        b2Vec2(-36.0f / PTM_RATIO, 19.6f / PTM_RATIO),
    };

    
    spriteShape.Set(verts, 4);
    fixtureDef.shape = &spriteShape;
    fixtureDef.density = 1.0f;
    fixtureDef.friction = 0.0f;
    fixtureDef.restitution = 0.0f;
    fixtureDef.filter.categoryBits = 0x01;
    fixtureDef.filter.maskBits = 0x01;
    body->CreateFixture(&fixtureDef);
    
    [candies addObject:[NSValue valueWithPointer:body]];
    
    return body;
}
-(b2Body *) createAnchorAt:(CGPoint)pt
{
    // Get the sprite from the sprite sheet
    CCSprite *sprite = [CCSprite spriteWithSpriteFrameName:@"pineapple.png"];
    [sprite addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(playerDragged:)]];
    sprite.isTouchEnabled = YES;
    [self addChild:sprite];
    
    
    // Defines the body of your candy
    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    bodyDef.position = b2Vec2(pt.x/PTM_RATIO, pt.y/PTM_RATIO);
    bodyDef.userData = sprite;
    bodyDef.linearDamping = 0.0f;
    bodyDef.angularDamping = 0.0f;
    
    b2Body *body = world->CreateBody(&bodyDef);
    
    // Define the fixture as a polygon
    b2FixtureDef fixtureDef;
    b2PolygonShape spriteShape;
    
    b2Vec2 verts[] = {
        b2Vec2(-7.6f / PTM_RATIO, -34.4f / PTM_RATIO),
        b2Vec2(8.3f / PTM_RATIO, -34.4f / PTM_RATIO),
        b2Vec2(15.55f / PTM_RATIO, -27.15f / PTM_RATIO),
        b2Vec2(13.8f / PTM_RATIO, 23.05f / PTM_RATIO),
        b2Vec2(-3.35f / PTM_RATIO, 35.25f / PTM_RATIO),
        b2Vec2(-16.25f / PTM_RATIO, 25.55f / PTM_RATIO),
        b2Vec2(-15.55f / PTM_RATIO, -23.95f / PTM_RATIO)
    };
    
    spriteShape.Set(verts, 7);
    fixtureDef.shape = &spriteShape;
    fixtureDef.density = 1.0f;
    fixtureDef.friction = 0.0f;
    fixtureDef.restitution = 0.0f;
    fixtureDef.filter.categoryBits = 0x01;
    fixtureDef.filter.maskBits = 0x01;
    body->CreateFixture(&fixtureDef);
    
    [candies addObject:[NSValue valueWithPointer:body]];
    
    return body;
}
- (void) addMonster {
    
    CCSprite * monster = [CCSprite spriteWithFile:@"planeta.png"];
    CCMotionStreak *missileStreak;
    // Determine where to spawn the monster along the Y axis
    CGSize winSize = [CCDirector sharedDirector].winSize;
    int minY = monster.contentSize.height / 2;
    int maxY = winSize.height - monster.contentSize.height/2;
    int rangeY = maxY - minY;
    int actualY = (arc4random() % rangeY) + minY;
    
    // Create the monster slightly off-screen along the right edge,
    // and along a random position along the Y axis as calculated above
    
    
    // Determine speed of the monster
    int minDuration = 4.0;
    int maxDuration = 8.0;
    int rangeDuration = maxDuration - minDuration;
    int actualDuration = (arc4random() % rangeDuration) + minDuration;
    
    // Create the actions
    
    
    
    CCCallBlockN * actionMoveDone = [CCCallBlockN actionWithBlock:^(CCNode *node) {
        [node removeFromParentAndCleanup:YES];
    }];
    
    if (self.counter%4==0) {
        ccBezierConfig bezier;
        bezier.controlPoint_1 = ccp(winSize.width/2 + 35, 310);
        bezier.endPosition = ccp(-monster.contentSize.width/2, actualY);
        id move1 = [CCBezierTo actionWithDuration:actualDuration bezier:bezier];
        [monster runAction:[CCSequence actions:move1,actionMoveDone,nil]];
        
        id move2 = [CCBezierTo actionWithDuration:actualDuration bezier:bezier];
        CCCallBlockN * actionMoveDone2 = [CCCallBlockN actionWithBlock:^(CCNode *node) {
            [node removeFromParentAndCleanup:YES];
        }];
        missileStreak = [CCMotionStreak streakWithFade:2 minSeg:5 width:20 color:ccc3(255,0,0) textureFilename:@"rope_texture2.png"];
        [missileStreak setPosition:monster.position];
        [missileStreak runAction:[CCSequence actions:move2,actionMoveDone2,nil]];
        [self addChild:missileStreak];
        
    }else if (self.counter%4==1){
        ccBezierConfig bezier1;
        bezier1.controlPoint_1 = ccp(winSize.width/2, 380);
        bezier1.controlPoint_2 = ccp(winSize.width/2 + 150, 310);
        bezier1.endPosition = ccp(-monster.contentSize.width/2, actualY);
        id move1 = [CCBezierTo actionWithDuration:actualDuration bezier:bezier1];
        [monster runAction:[CCSequence actions:move1,actionMoveDone,nil]];
        
        id move2 = [CCBezierTo actionWithDuration:actualDuration bezier:bezier1];
        CCCallBlockN * actionMoveDone2 = [CCCallBlockN actionWithBlock:^(CCNode *node) {
            [node removeFromParentAndCleanup:YES];
        }];
        missileStreak = [CCMotionStreak streakWithFade:2 minSeg:5 width:20 color:ccc3(0,255,0) textureFilename:@"rope_texture2.png"];
        [missileStreak setPosition:monster.position];
        [missileStreak runAction:[CCSequence actions:move2,actionMoveDone2,nil]];
        [self addChild:missileStreak];
        
    }else if (self.counter%4==2){
        monster.position = ccp(winSize.width + monster.contentSize.width/2, actualY);
        
        CCMoveTo * actionMove = [CCMoveTo actionWithDuration:actualDuration
                                                    position:ccp(-monster.contentSize.width/2, actualY)];
        CCCallBlockN * actionMoveDone = [CCCallBlockN actionWithBlock:^(CCNode *node) {
            [node removeFromParentAndCleanup:YES];
        }];
        [monster runAction:[CCSequence actions:actionMove, actionMoveDone, nil]];
        
        id move2 = [CCMoveTo actionWithDuration:actualDuration
                                       position:ccp(-monster.contentSize.width/2, actualY)];
        CCCallBlockN * actionMoveDone2 = [CCCallBlockN actionWithBlock:^(CCNode *node) {
            [node removeFromParentAndCleanup:YES];
        }];
        missileStreak = [CCMotionStreak streakWithFade:2 minSeg:5 width:20 color:ccc3(127,127,255) textureFilename:@"rope_texture2.png"];
        [missileStreak setPosition:monster.position];
        [missileStreak runAction:[CCSequence actions:move2,actionMoveDone2,nil]];
        [self addChild:missileStreak];
    }else{
        monster.position = ccp(winSize.width + monster.contentSize.width/2, actualY);
        ccBezierConfig bezier1;
        bezier1.controlPoint_1 = ccp(winSize.width + monster.contentSize.width/2, actualY);
        bezier1.controlPoint_2 = ccp(winSize.width/2 + 150, 310);
        bezier1.endPosition = ccp(-monster.contentSize.width/2, actualY);
        id move1 = [CCBezierTo actionWithDuration:actualDuration bezier:bezier1];
        [monster runAction:[CCSequence actions:move1,actionMoveDone,nil]];
        
        id move2 = [CCBezierTo actionWithDuration:actualDuration bezier:bezier1];
        CCCallBlockN * actionMoveDone2 = [CCCallBlockN actionWithBlock:^(CCNode *node) {
            [node removeFromParentAndCleanup:YES];
        }];
        missileStreak = [CCMotionStreak streakWithFade:2 minSeg:5 width:20 color:ccc3(255,255,255) textureFilename:@"rope_texture2.png"];
        [missileStreak setPosition:monster.position];
        [missileStreak runAction:[CCSequence actions:move2,actionMoveDone2,nil]];
        [self addChild:missileStreak];
    }
//    monster.tag = 1;
    // MISSILE TRAIL CODE START HERE
    
    [self addChild:monster];
    [_monsters addObject:monster];
    [_tails addObject:missileStreak];
    self.counter++;
    
}

-(void)gameLogic:(ccTime)dt {
    [self addMonster];
}
-(id) init
{
	if ((self = [super init])) {
		
		// enable events
		_monsters = [[NSMutableArray alloc] init];
        _tails = [[NSMutableArray alloc] init];
        ropes = [[NSMutableArray alloc] init];
        candies = [[NSMutableArray alloc] init];
        ropeSpriteSheet = [CCSpriteBatchNode batchNodeWithFile:@"rope_texture.png"];
		
        // Load the sprite sheet into the sprite cache
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"CutTheVerlet.plist"];
        [self addChild:ropeSpriteSheet];
		self.touchEnabled = YES;
		CGSize s = [CCDirector sharedDirector].winSize;
		
		// init physics
		[self initPhysics];
		
		// create reset button
		[self createMenu];
		
		//Set up sprite
        [self schedule:@selector(gameLogic:) interval:1.0];
        [self schedule:@selector(update:)];
        CCSprite *background = [CCSprite spriteWithFile:@"BgLevel.png"];
        background.anchorPoint = CGPointZero;
        [self addChild:background z:-1];

        self.lives = 5;
        
        self.labelScore = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"LIVES: %d",self.lives] fontName:@"Marker Felt" fontSize:16];
        [self.labelScore setColor:ccc3(255,255,255)];
        [self.labelScore setPosition:ccp(s.width*0.9, s.height *0.9)];
        [self addChild:self.labelScore];
        
        #define cc_to_b2Vec(x,y)   (b2Vec2((x)/PTM_RATIO, (y)/PTM_RATIO))
        b2Body *body1 = [self createCandyAt:CGPointMake(s.width * 0.5, s.height * 0.7)];
        self.theAstro = body1;
        b2Body *body2 = [self createAnchorAt:CGPointMake(s.width * 0.7, s.height * 0.1)];
        self.theRope = body2;
        [self createRopeWithBodyA:body1 anchorA:body1->GetLocalCenter()
                            bodyB:body2 anchorB:body2->GetLocalCenter()
                              sag:1.1];
        
		
		[self scheduleUpdate];
        
        int n = 10 * 60;
        int32 velocityIterations = 8;
        int32 positionIterations = 1;
        float32 dt = 1.0 / 60.0;
        while (n--)
        {
            // Instruct the world to perform a single step of simulation.
            world->Step(dt, velocityIterations, positionIterations);
            for (VRope *rope in ropes)
            {
                [rope update:dt];
            }
        }
        
        // This last update takes care of the texture repositioning.
        [self update:dt];
	}
	return self;
}
-(void) createRopeWithBodyA:(b2Body*)bodyA anchorA:(b2Vec2)anchorA
                      bodyB:(b2Body*)bodyB anchorB:(b2Vec2)anchorB
                        sag:(float32)sag
{
    b2RopeJointDef jd;
    jd.bodyA = bodyA;
    jd.bodyB = bodyB;
    jd.localAnchorA = anchorA;
    jd.localAnchorB = anchorB;
    
    // Max length of joint = current distance between bodies * sag
    float32 ropeLength = (bodyA->GetWorldPoint(anchorA) - bodyB->GetWorldPoint(anchorB)).Length() * sag;
    jd.maxLength = ropeLength;
    
    // Create joint
    b2RopeJoint *ropeJoint = (b2RopeJoint *)world->CreateJoint(&jd);
    
    VRope *newRope = [[VRope alloc] initWithRopeJoint:ropeJoint spriteSheet:ropeSpriteSheet];
    
    [ropes addObject:newRope];
    [newRope release];
}
-(void) dealloc
{
	delete world;
	world = NULL;
	
	delete m_debugDraw;
	m_debugDraw = NULL;
	[_monsters release];
    _monsters = nil;
	[super dealloc];
}	

-(void) createMenu
{
    CCMenuItemImage *menuItem = [CCMenuItemImage itemWithNormalImage:@"pause.png" selectedImage:@"pause.png" block:^(id sender) {
        [[CCDirector sharedDirector] pause];
        MenuLayer *menu = [[MenuLayer alloc]init];
        [self addChild:menu];
    }];
	CCMenuItem * menuItem2 = [CCMenuItemFont itemWithString:@"Menu" block:^(id sender) {
        NSLog(@"MENU CLICKED");
        [[CCDirector sharedDirector] pause];
        MenuLayer *menu = [[MenuLayer alloc]init];
        [self addChild:menu];

//        CCScene *pauseScene = [CCScene node];
//        pauseScene.contentSize = CGSizeMake(200, 200);
//        
//        // 'layer' is an autorelease object.
//        MenuLayer *layer = [MenuLayer node];
//        
//        // add layer as a child to scene
//        [pauseScene addChild: layer];
//        [[CCDirector sharedDirector] pushScene:pauseScene];
    }];
    

	CCMenu * myMenu = [CCMenu menuWithItems: menuItem, nil];
    CGSize size = [[CCDirector sharedDirector] winSize];
    [myMenu alignItemsHorizontallyWithPadding:20];
    
    [myMenu setPosition:ccp( size.width*0.1, size.height*0.1)];
	// add the menu to your scene
	[self addChild:myMenu];
}

-(void) initPhysics
{
	
	CGSize s = [[CCDirector sharedDirector] winSize];
	
	b2Vec2 gravity;
	gravity.Set(0.0f, 0.0f);
	world = new b2World(gravity);
	
	
	// Do we want to let bodies sleep?
	world->SetAllowSleeping(true);
	
	world->SetContinuousPhysics(true);
	
	m_debugDraw = new GLESDebugDraw( PTM_RATIO );
	world->SetDebugDraw(m_debugDraw);
	
	uint32 flags = 0;
	flags += b2Draw::e_shapeBit;
	//		flags += b2Draw::e_jointBit;
	//		flags += b2Draw::e_aabbBit;
	//		flags += b2Draw::e_pairBit;
	//		flags += b2Draw::e_centerOfMassBit;
	m_debugDraw->SetFlags(flags);		
	
	
	// Define the ground body.
	b2BodyDef groundBodyDef;
	groundBodyDef.position.Set(0, 0); // bottom-left corner
	
	// Call the body factory which allocates memory for the ground body
	// from a pool and creates the ground box shape (also from a pool).
	// The body is also added to the world.
	groundBody = world->CreateBody(&groundBodyDef);
	
	// Define the ground box shape.
	b2EdgeShape groundBox;		
	
	// bottom
	
	groundBox.Set(b2Vec2(0,0), b2Vec2(s.width/PTM_RATIO,0));
	groundBody->CreateFixture(&groundBox,0);
	
	// top
	groundBox.Set(b2Vec2(0,s.height/PTM_RATIO), b2Vec2(s.width/PTM_RATIO,s.height/PTM_RATIO));
	groundBody->CreateFixture(&groundBox,0);
	
	// left
	groundBox.Set(b2Vec2(0,s.height/PTM_RATIO), b2Vec2(0,0));
	groundBody->CreateFixture(&groundBox,0);
	
	// right
	groundBox.Set(b2Vec2(s.width/PTM_RATIO,s.height/PTM_RATIO), b2Vec2(s.width/PTM_RATIO,0));
	groundBody->CreateFixture(&groundBox,0);
    
    
    
//    (s.width * 0.9, s.height * 0.1)
    // Define the ground box shape.
//	b2EdgeShape groundBox2;
//	
//	// bottom
//	
//	groundBox2.Set(b2Vec2(s.width/PTM_RATIO*0.9,0), b2Vec2(s.width/PTM_RATIO,0));
//	groundBody->CreateFixture(&groundBox2,0);
//	
//	// top
//	groundBox2.Set(b2Vec2(s.width/PTM_RATIO*0.9,s.height * 0.15/PTM_RATIO), b2Vec2(s.width/PTM_RATIO,s.height * 0.15/PTM_RATIO));
//	groundBody->CreateFixture(&groundBox2,0);
//	
//	// left
//	groundBox2.Set(b2Vec2(s.width/PTM_RATIO*0.9,s.height * 0.15/PTM_RATIO), b2Vec2(s.width/PTM_RATIO*0.9,0));
//	groundBody->CreateFixture(&groundBox2,0);
//	
//	// right
//	groundBox2.Set(b2Vec2(s.width/PTM_RATIO,s.height * 0.15/PTM_RATIO), b2Vec2(s.width/PTM_RATIO,0));
//	groundBody->CreateFixture(&groundBox2,0);
}

-(void) draw
{
	//
	// IMPORTANT:
	// This is only for debug purposes
	// It is recommend to disable it
	//
	[super draw];
	
	ccGLEnableVertexAttribs( kCCVertexAttribFlag_Position );
	
	kmGLPushMatrix();
	
	world->DrawDebugData();	
	
	kmGLPopMatrix();
}

//-(b2Body *) addNewSpriteAtPosition:(CGPoint)p
//{
//	CCLOG(@"Add sprite %0.2f x %02.f",p.x,p.y);
//	// Define the dynamic body.
//	//Set up a 1m squared box in the physics world
//	b2BodyDef bodyDef;
//	bodyDef.type = b2_dynamicBody;
//	bodyDef.position.Set(p.x/PTM_RATIO, p.y/PTM_RATIO);
//	b2Body *body = world->CreateBody(&bodyDef);
//	
//	// Define another box shape for our dynamic body.
//	b2PolygonShape dynamicBox;
//	dynamicBox.SetAsBox(.5f, .5f);//These are mid points for our 1m box
//	
//	// Define the dynamic body fixture.
//	b2FixtureDef fixtureDef;
//	fixtureDef.shape = &dynamicBox;	
//	fixtureDef.density = 1.0f;
//	fixtureDef.friction = 0.3f;
//	body->CreateFixture(&fixtureDef);
//	
//
//	CCNode *parent = [self getChildByTag:kTagParentNode];
//	
//	//We have a 64x64 sprite sheet with 4 different 32x32 images.  The following code is
//	//just randomly picking one of the images
//	int idx = (CCRANDOM_0_1() > .5 ? 0:1);
//	int idy = (CCRANDOM_0_1() > .5 ? 0:1);
//    
//    CGSize winSize = [CCDirector sharedDirector].winSize;
//    self.rope = [CCSprite spriteWithSpriteFrameName:@"pineapple.png"];
//    self.rope.position = ccp(winSize.width - self.rope.contentSize.width/2, self.rope.contentSize.height/2);
//    self.rope.isTouchEnabled = YES;
////	CCPhysicsSprite *sprite = [CCPhysicsSprite spriteWithTexture:spriteTexture_ rect:CGRectMake(32 * idx,32 * idy,32,32)];
//	[parent addChild:self.rope];
//	[self.rope addGestureRecognizer:[[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(playerDragged:)]];
////	[sprite setPTMRatio:PTM_RATIO];
////	[sprite setB2Body:body];
//	[self.rope setPosition: ccp( p.x, p.y)];
//    
//    return body;
//
//}
- (void)playerDragged:(UIPanGestureRecognizer *)recognizer {
    
    CGPoint p;
    CGPoint v;
    
    switch( recognizer.state ) {
        case UIGestureRecognizerStatePossible:
        case UIGestureRecognizerStateBegan:
//            NSLog(@"DRAG BEGUN");
            self.oldCoor = [recognizer locationInView:[CCDirector sharedDirector].openGLView];
            break;
        case UIGestureRecognizerStateFailed:
            break;
        case UIGestureRecognizerStateEnded:
            break;
        case UIGestureRecognizerStateCancelled:
            break;
        case UIGestureRecognizerStateChanged:
//            NSLog(@"DRAG CHANGED");
            
            p = [recognizer locationInView:[CCDirector sharedDirector].openGLView];
            float deltaX = p.x - self.oldCoor.x;
            float deltaY = p.y - self.oldCoor.y;
            CGPoint cocosCoordinates = ccp( p.x * PTM_RATIO, p.y * PTM_RATIO );
            CGPoint location = ccp( cocosCoordinates.x, cocosCoordinates.y);

            CGSize s = [[CCDirector sharedDirector] winSize];
            // This is where the magic happens
            
            b2Vec2 gravity;
            gravity.Set(deltaX/PTM_RATIO, -deltaY/PTM_RATIO);
//            self.theAstro->ApplyForceToCenter(gravity);
            self.theRope->ApplyForceToCenter(gravity);
//            self.theRope->SetTransform(b2Vec2((self.oldCoor.x+deltaX)/PTM_RATIO,(s.height-self.oldCoor.y- deltaY)/PTM_RATIO), 0);




            break;
        
    }
    
}
-(void) update: (ccTime) dt
{
	//It is recommended that a fixed time step is used with Box2D for stability
	//of the simulation, however, we are using a variable time step here.
	//You need to make an informed choice, the following URL is useful
	//http://gafferongames.com/game-physics/fix-your-timestep/
	
	int32 velocityIterations = 8;
	int32 positionIterations = 1;
	
	// Instruct the world to perform a single step of simulation. It is
	// generally best to keep the time step and iterations fixed.
	world->Step(dt, velocityIterations, positionIterations);
    
    for (b2Body* b = world->GetBodyList(); b; b = b->GetNext())
    {
        CCSprite *myActor = (CCSprite*)b->GetUserData();
        if (myActor)
        {
            //Synchronize the AtlasSprites position and rotation with the corresponding body
            myActor.position = CGPointMake( b->GetPosition().x * PTM_RATIO, b->GetPosition().y * PTM_RATIO);
            myActor.rotation = -1 * CC_RADIANS_TO_DEGREES(b->GetAngle());
        }
    }
    
    // Update all the ropes
    for (VRope *rope in ropes)
    {
        [rope update:dt];
        [rope updateSprites];
    }
    
    NSMutableArray *monstersToDelete = [[NSMutableArray alloc] init];
    NSMutableArray *tailsToDelete = [[NSMutableArray alloc] init];
    int i = 0;
    for (CCSprite *monster in _monsters) {
        CCSprite *tail = [_tails objectAtIndex:i];
        if (CGRectIntersectsRect(self.rope.boundingBox, monster.boundingBox)) {
            [monstersToDelete addObject:monster];
            [tailsToDelete addObject:tail];
            NSLog(@"HIT");
            self.lives--;
            if (self.lives == 0) {
                [[CCDirector sharedDirector] pause];
                LoseLayer *menu = [[LoseLayer alloc]init];
                [self addChild:menu];
            }else{
                [self.labelScore setString:[NSString stringWithFormat:@"LIVES: %d",self.lives]];
            }
            
        }
        i++;
    }
//    for (CCSprite *tail in _tails) {
//        
//        if (CGRectIntersectsRect(self.rope.boundingBox, tail.boundingBox)) {
//            [tailsToDelete addObject:tail];
//            NSLog(@"HITZ");
//        }
//    }
    
//    for (int i=0; i< [_monsters count]; i++) {
//        CCSprite *monster = [_monsters objectAtIndex:i];
//        CCSprite *tail    = [_tails objectAtIndex:i];
//        if (CGRectIntersectsRect(self.rope.boundingBox, monster.boundingBox)) {
//            [monstersToDelete addObject:monster];
//            [tailsToDelete addObject:tail];
//            NSLog(@"HIT");
//            self.lives--;
//            if (self.lives == 0) {
//                [[CCDirector sharedDirector] pause];
//                LoseLayer *menu = [[LoseLayer alloc]init];
//                [self addChild:menu];
//            }else{
//                [self.labelScore setString:[NSString stringWithFormat:@"LIVES: %d",self.lives]];
//            }
//            
//        }
//    }
//    for (int j=0; j< [monstersToDelete count]; j++) {
//        CCSprite *monster = [_monsters objectAtIndex:j];
//        CCSprite *tail    = [_tails objectAtIndex:j];
//        
//        [_monsters removeObject:monster];
//        [_tails removeObject:tail];
//        
//        [self removeChild:monster cleanup:YES];
//        [self removeChild:tail cleanup:YES];
//    }
    for (CCSprite *monster in monstersToDelete) {
        [_monsters removeObject:monster];
        [self removeChild:monster cleanup:YES];
    }
    for (CCSprite *tail in tailsToDelete) {
        [_tails removeObject:tail];
        [self removeChild:tail cleanup:YES];
    }
    [tailsToDelete release];
    [monstersToDelete release];
    

}

- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
	//Add a new body/atlas sprite at the touched location
//	for( UITouch *touch in touches ) {
//		CGPoint location = [touch locationInView: [touch view]];
//		
//		location = [[CCDirector sharedDirector] convertToGL: location];
//		
//		[self addNewSpriteAtPosition: location];
//	}
    
    static CGSize s = [[CCDirector sharedDirector] winSize];
    
    UITouch *touch = [touches anyObject];
    CGPoint pt0 = [touch previousLocationInView:[touch view]];
    CGPoint pt1 = [touch locationInView:[touch view]];
    
    // Correct Y axis coordinates to cocos2d coordinates
    pt0.y = s.height - pt0.y;
    pt1.y = s.height - pt1.y;
    
    for (VRope *rope in ropes)
    {
        for (VStick *stick in rope.sticks)
        {
            CGPoint pa = [[stick getPointA] point];
            CGPoint pb = [[stick getPointB] point];
            
            if ([self checkLineIntersection:pt0 :pt1 :pa :pb])
            {
                 NSLog(@"WOI");
                // Cut the rope here
               
                return;
            }
        }
    }
}
-(BOOL)checkLineIntersection:(CGPoint)p1 :(CGPoint)p2 :(CGPoint)p3 :(CGPoint)p4
{
    // http://local.wasp.uwa.edu.au/~pbourke/geometry/lineline2d/
    CGFloat denominator = (p4.y - p3.y) * (p2.x - p1.x) - (p4.x - p3.x) * (p2.y - p1.y);
    
    // In this case the lines are parallel so you assume they don't intersect
    if (denominator == 0.0f)
        return NO;
    CGFloat ua = ((p4.x - p3.x) * (p1.y - p3.y) - (p4.y - p3.y) * (p1.x - p3.x)) / denominator;
    CGFloat ub = ((p2.x - p1.x) * (p1.y - p3.y) - (p2.y - p1.y) * (p1.x - p3.x)) / denominator;
    
    if (ua >= 0.0 && ua <= 1.0 && ub >= 0.0 && ub <= 1.0)
    {
        return YES;
    }
    
    return NO;
}
#pragma mark GameKit delegate

-(void) achievementViewControllerDidFinish:(GKAchievementViewController *)viewController
{
	AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
	[[app navController] dismissModalViewControllerAnimated:YES];
}

-(void) leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController
{
	AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
	[[app navController] dismissModalViewControllerAnimated:YES];
}

@end
