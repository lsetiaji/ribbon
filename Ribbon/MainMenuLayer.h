//
//  MainMenuLayer.h
//  Ribbon
//
//  Created by Esa Firman on 1/29/14.
//  Copyright 2014 Dycode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface MainMenuLayer : CCLayer {
    
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;
@end
