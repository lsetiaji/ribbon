//
//  AboutLayer.mm
//  Ribbon
//
//  Created by Esa Firman on 1/30/14.
//  Copyright 2014 Dycode. All rights reserved.
//

#import "AboutLayer.h"


@implementation AboutLayer
+(CCScene *) scene
{
	// 'scene' is an autorelease object.
	CCScene *scene = [CCScene node];
	
	// 'layer' is an autorelease object.
	AboutLayer *layer = [AboutLayer node];
	
	// add layer as a child to scene
	[scene addChild: layer];
	
	// return the scene
	return scene;
}
-(id) init
{
	if( (self=[super init])) {
		
		// ask director for the window size
		CGSize size = [[CCDirector sharedDirector] winSize];
		
		CCSprite *background;
        background = [CCSprite spriteWithFile:@"BackgroundAbout.png"];
		background.position = ccp(size.width/2, size.height/2);
        
        CCSprite *logo;
        logo = [CCSprite spriteWithFile:@"LogoAbout.png"];
		logo.position = ccp(size.width/2, size.height*0.8);
        
        
		
		// add the label as a child to this Layer
        [self addChild:background z:-1];
        [self addChild:logo z:0];
        [self setUpMenus];
	}
	
	return self;
}
-(void) setUpMenus
{
	// Create some menu items
    CCMenuItemImage *menuItem1 = [CCMenuItemImage itemWithNormalImage:@"ButtonShare.png"
                                                        selectedImage:@"ButtonShare.png"
                                                               target:self
                                                             selector:@selector(onClick:)];
    CCMenuItemImage *menuItem2 = [CCMenuItemImage itemWithNormalImage:@"ButtonRate.png"
                                                        selectedImage:@"ButtonRate.png"
                                                               target:self
                                                             selector:@selector(onClick:)];
    CCMenuItemImage *menuItem3 = [CCMenuItemImage itemWithNormalImage:@"ButtonSupport.png"
                                                        selectedImage:@"ButtonSupport.png"
                                                               target:self
                                                             selector:@selector(onClick:)];
    
    
	// Create a menu and add your menu items to it
	CCMenu * myMenu = [CCMenu menuWithItems: menuItem1,menuItem2,menuItem3, nil];

    CGSize size = [[CCDirector sharedDirector] winSize];
    [myMenu alignItemsVerticallyWithPadding:10];
    [myMenu setPosition:ccp( size.width/2, size.height*0.3)];
	// add the menu to your scene
	[self addChild:myMenu];
    
    CCMenuItemImage *menuItem4 = [CCMenuItemImage itemWithNormalImage:@"ButtonBack.png"
                                                        selectedImage:@"ButtonBack.png"
                                                               target:self
                                                             selector:@selector(backClicked)];
    CCMenu * myMenu2 = [CCMenu menuWithItems: menuItem4, nil];
    [myMenu2 setPosition:ccp(size.width*0.1, size.height*0.9)];
    [self addChild:myMenu2];
    
}
-(void) backClicked
{
    [[CCDirector sharedDirector] popScene];
}
@end
