//
//  LoseLayer.mm
//  Ribbon
//
//  Created by Esa Firman on 2/3/14.
//  Copyright 2014 Dycode. All rights reserved.
//

#import "LoseLayer.h"
#import "LevelsLayer.h"
#import "HelloWorldLayer.h"
@implementation LoseLayer
-(id) init
{
	if ((self = [super init])) {
        CGSize size = [[CCDirector sharedDirector] winSize];
        CCSprite *bg = [CCSprite spriteWithFile:@"BgLose.png"];
        bg.position = ccp(size.width/2, size.height/2);
        [self addChild:bg z:-1];
        
        CCLabelTTF *label1 = [CCLabelTTF labelWithString:@"SOMETHING WAS WRONG!" fontName:@"Marker Felt" fontSize:18];
        [label1 setColor:ccc3(255,255,255)];
        [label1 setPosition:ccp(size.width/2, size.height *0.73)];
        [self addChild:label1];
        
        CCLabelTTF *labelScore = [CCLabelTTF labelWithString:@"SCORE: 4824" fontName:@"Marker Felt" fontSize:16];
        [labelScore setColor:ccc3(255,255,255)];
        [labelScore setPosition:ccp(size.width/2, size.height *0.68)];
        [self addChild:labelScore];
        
        [self setUpMenus];
    }
    
    return self;
}

-(void) setUpMenus
{
    
	// Create some menu items
    CCMenuItemImage *menuItem1 = [CCMenuItemImage itemWithNormalImage:@"ButtonMenu.png" selectedImage:@"ButtonMenu.png" block:^(id sender) {
        NSLog(@"MENU CLICKED");
        [self removeFromParentAndCleanup:YES];
        [[CCDirector sharedDirector] resume];
        
        CCScene * levelSelector = [CCScene node];
        LevelsLayer *layer = [LevelsLayer node];
        
        [levelSelector addChild: layer];
        [[CCDirector sharedDirector] replaceScene:levelSelector];
    }];
    CCMenuItemImage *menuItem2 = [CCMenuItemImage itemWithNormalImage:@"ButtonRetry.png" selectedImage:@"ButtonRetry.png" block:^(id sender) {
        CCScene * levelSelector = [CCScene node];
        
        // 'layer' is an autorelease object.
        [self removeFromParentAndCleanup:YES];
        [[CCDirector sharedDirector] resume];
        HelloWorldLayer *layer = [HelloWorldLayer node];
        
        // add layer as a child to scene
        [levelSelector addChild: layer];
        [[CCDirector sharedDirector] replaceScene:levelSelector];
    }];
    //    CCLabelTTF *labelSound = [CCLabelTTF labelWithString:@"Sound Off" fontName:@"Marker Felt" fontSize:32];
//    CCMenuItemImage *menuItem3 = [CCMenuItemImage itemWithNormalImage:@"ButtonMute.png" selectedImage:@"ButtonMute.png" block:^(id sender) {
//        [SimpleAudioEngine sharedEngine].mute = ![SimpleAudioEngine sharedEngine].mute;
//        CCMenuItemImage *test = (CCMenuItemImage *)sender;
//        if ([SimpleAudioEngine sharedEngine].mute) {
//            CCSprite *frame = [CCSprite spriteWithFile:@"ButtonSound.png"];
//            [test setNormalImage:frame];
//            //            [labelSound setString:@"Sound On"];
//        }else
//        {
//            CCSprite *frame = [CCSprite spriteWithFile:@"ButtonMute.png"];
//            [test setNormalImage:frame];
//            //            [labelSound setString:@"Sound Off"];
//        }
//    }];
    
    
    
    CCMenuItemImage *menuItem4 = [CCMenuItemImage itemWithNormalImage:@"ButtonContinue.png" selectedImage:@"ButtonContinue.png" block:^(id sender) {
        [self removeFromParentAndCleanup:YES];
        [[CCDirector sharedDirector] resume];
    }];
    
    
	CCMenu * myMenu = [CCMenu menuWithItems: menuItem1,menuItem2, nil];
	// Arrange the menu items vertically
    [myMenu alignItemsHorizontallyWithPadding:10];
    CGSize size = [[CCDirector sharedDirector] winSize];
    [myMenu setPosition:ccp(size.width/2 - menuItem2.boundingBox.size.width, size.height *0.2)];
	// add the menu to your scene
	[self addChild:myMenu];
    //    [labelSound setColor:ccc3(255,255,255)];
    //    labelSound.position = ccp( menuItem3.boundingBox.size.width, menuItem3.boundingBox.size.height);
    //    [self addChild:labelSound z:100];
}
@end
